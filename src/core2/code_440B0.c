#include <ultra64.h>
#include "functions.h"
#include "variables.h"

void func_802CBAAC(Actor *this);

/* .data */
extern ActorAnimationInfo D_80367010[];
extern ActorAnimationInfo D_80367060[];
extern ActorInfo D_803670B8 = { 0x13, 0x67, 0x358, 0x1, D_80367010, func_802CBAAC, func_80326224, func_80325888, { 0x7, 0x6C}, 0, 0.8f, { 0x0, 0x0, 0x0, 0x0}};
extern ActorInfo D_803670DC = { 0xDD, 0xF2, 0x566, 0x1, D_80367010, func_802CBAAC, func_80326224, func_80325888, { 0x7, 0x6C}, 0, 0.8f, { 0x0, 0x0, 0x0, 0x0}};
extern ActorInfo D_80367100 = { 0x13, 0xF5, 0x38F, 0x1, D_80367060, func_802CBAAC, func_80326224, func_80325888, { 0x7, 0x6C}, 0, 0.8f, { 0x0, 0x0, 0x0, 0x0}};

/* .code */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CB040.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CB078.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CB140.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CB1A4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CB22C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CB310.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CB3C8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CB4B8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CB5A8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CB6A0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CB6E4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CB76C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CB7C0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CBA34.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_440B0/func_802CBAAC.s")
