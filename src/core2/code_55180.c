#include <ultra64.h>
#include "functions.h"
#include "variables.h"

Actor *func_802DC320(ActorMarker *marker, Gfx **gfx, Mtx **mtx, Vtx **vtx);
void func_802DC45C(Actor *this);

/* .data */
extern ActorInfo D_8036804C = {
    0x174, 0x1DB, 0x54C, 
    0x1, NULL, 
    func_802DC45C, func_80326224, func_802DC320,
    { 0x0, 0x0}, 0, 0.0f, { 0x0, 0x0, 0x0, 0x0}
};

/* .code */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55180/func_802DC110.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55180/func_802DC188.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55180/func_802DC208.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55180/func_802DC320.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55180/func_802DC430.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55180/func_802DC45C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55180/func_802DC4C4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55180/func_802DC528.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55180/func_802DC560.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55180/func_802DC5B8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55180/func_802DC604.s")
