#include <ultra64.h>
#include "functions.h"
#include "variables.h"

void func_802CF83C(Actor *this);
Actor *func_802CEBFC(ActorMarker *marker, Gfx **gfx, Mtx **mtx, Vtx **vtx);

extern ActorInfo D_80367310 = {0x217, ACTOR_34D_BEE_SWARM, 0x49E, 
    1, NULL, 
    func_802CF83C, NULL, func_802CEBFC,
    {0,0}, 0, 1.0f, {0,0,0,0}
}; 

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CEB60.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CEBA8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CEBFC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CEDE4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CEEA4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CEF54.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CF040.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CF174.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CF1C8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CF434.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CF518.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CF57C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CF5E4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CF610.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CF7CC.s")

void func_802CF83C(Actor *this);
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_47BD0/func_802CF83C.s")
