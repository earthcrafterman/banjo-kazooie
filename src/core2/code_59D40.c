#include <ultra64.h>
#include "functions.h"
#include "variables.h"

void func_802E1168(Actor *this);

/* .data */
extern ActorAnimationInfo D_803685D0[];
extern ActorInfo D_80368620 = { 
    0x14, 0x68, 0x3B0,
    0x1, D_803685D0,
    func_802E1168, func_80326224, func_80325888,
    { 0x0, 0x0}, 0, 0.0f, { 0x0, 0x0, 0x0, 0x0}
};

/* .code */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59D40/func_802E0CD0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59D40/func_802E0CE0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59D40/func_802E0DC0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59D40/func_802E0E88.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59D40/func_802E0EC8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59D40/func_802E0F60.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59D40/func_802E0FC4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59D40/func_802E1010.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59D40/func_802E1050.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59D40/func_802E10F0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59D40/func_802E1168.s")

/* BREAK??? */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59D40/func_802E1790.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59D40/func_802E17E8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59D40/func_802E1A04.s")
