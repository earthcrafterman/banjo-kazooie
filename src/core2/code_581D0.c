#include <ultra64.h>
#include "functions.h"
#include "variables.h"

void func_802DF2C4(Actor *this);

/* .data */
extern ActorInfo D_8036838C = { 
    0x17B, 0x2B5, 0x472,
    0, NULL, 
    func_802DF2C4, func_80326224, func_80325340, 
    { 0x0, 0x0}, 0, 0.0f, { 0x0, 0x0, 0x0, 0x0}
};

/* .code */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_581D0/func_802DF160.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_581D0/func_802DF270.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_581D0/func_802DF2B4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_581D0/func_802DF2C4.s")
