#include <ultra64.h>
#include "functions.h"
#include "variables.h"

void func_802E0B10(Actor *this);

/* .data */
extern ActorInfo D_803685A0 = { 
    0x39, 0x2D, 0x41A,
    0, NULL, 
    func_802E0B10, func_80326224, func_80325934,
    { 0x7, 0xD0}, 0, 0.0f, { 0x0, 0x0, 0x0, 0x0}
};


/* .code */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59A80/func_802E0A10.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59A80/func_802E0A90.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59A80/func_802E0B10.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59A80/func_802E0CB0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_59A80/func_802E0CB8.s")
