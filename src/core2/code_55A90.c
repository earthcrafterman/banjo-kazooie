#include <ultra64.h>
#include "functions.h"
#include "variables.h"

void func_802DCA30(Actor *this);

/* .data */
extern ActorInfo D_803680DC = { 
    0x176, 0x1DD, 0x54E, 
    0x1, NULL, 
    func_802DCA30, func_80326224, func_802DC7E0, 
    { 0x0, 0x0}, 0, 0.0f, { 0x0, 0x0, 0x0, 0x0}
};

/* .code */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55A90/func_802DCA20.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55A90/func_802DCA30.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55A90/func_802DCA90.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55A90/func_802DCAD4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_55A90/func_802DCB0C.s")
